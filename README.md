# Reviewing and Analyzing Scalable Techniques for Network Visualization - a Gitlab story



## Getting started

This is a repo containing all files and artifacts that got produced during the undertaking of the thesis by the same name as the repo title. Hopefully this can provide some guidance into how you, dear reader, may attempt to replicate the work presentend in the main paper.

## What are these files?
Within the data folder, you will find the csv file that was used to generate the graphs. In the main folder, a similar looking excel table can be found and that file has been iterated over multiple times and was the origination point of the csv.

A learnt lesson was to keep your csv files consistent, though we could not come up with a solution as we worked, there was a lot of growing pains between working on Google Sheets and the csv file locally. Finding a way to harmonize the two or simply always work on one file would be best and can avoid many headaches.

Another learnt lesson is to dedicate some time to set up your sheet with basic statistical measures (percentages, averages, medians, modes and so on...) from the get-go rather than doing it later by hand.

The ipynb contains the main code for the visualizations and, additionally, some visualizations that didn't make the cut. ChatGPT was used to help with the tweaking and generation as neither author was particularly experienced using matplotlib or sns.

Not all columns were used, some columns ended up being identical or superflous to the aim of the work, as such

| Title | Author | Year | Sizes noted in paper | Nodes avg | Edges avg | Density avg | Directed/Undirected | GRAPH FACTORS | Type | Static/Dynamic | Attributes | Structure | Layout | Real/Generated | STUDY DESIGN | Number participants | Nature participants | Within/between | HCI FACTORS | Type of task | Type of interaction |   | General category | Application Area | Improvement relates to |
|-------|--------|------|----------------------|-----------|-----------|-------------|---------------------|---------------|------|----------------|------------|-----------|--------|----------------|--------------|---------------------|---------------------|----------------|-------------|--------------|---------------------|---|------------------|------------------|------------------------|

